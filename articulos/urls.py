from django.urls import path
from .views import Lista, comprar,ReporteArticulosPDF

app_name = 'articulos'

urlpatterns = [
    path('lista/', Lista.as_view(), name='lista'),
    path('comprar/<int:pk>', comprar, name='comprar'),
    path('reporte_articulos_pdf/',ReporteArticulosPDF.as_view(), name="reporte_articulos_pdf"),
]
