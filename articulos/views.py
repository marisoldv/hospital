from django.shortcuts import render, redirect, get_object_or_404
from django.views.generic.list import ListView, View
from .models import Articulo
from django.contrib.auth.mixins import PermissionRequiredMixin, LoginRequiredMixin, AccessMixin
from django.conf import settings
from io import BytesIO
from reportlab.pdfgen import canvas
from django.http import FileResponse
from django.http import HttpResponse
import io
from reportlab.lib import colors 
from reportlab.lib.units import cm
from reportlab.platypus import (SimpleDocTemplate, PageBreak, Image, Spacer,Paragraph, Table, TableStyle)

class Lista(LoginRequiredMixin, ListView):
    model = Articulo
    paginate_by = 5

def comprar(request, pk):
    articulo = get_object_or_404(Articulo, pk=pk)
    if articulo.stock > 0:
        articulo.stock = articulo.stock - 1
        articulo.save()

        id = str(pk)

        request.session['total'] = request.session['total'] + float(articulo.precio)
        request.session['cuantos'] = request.session['cuantos'] + 1

        if id in request.session['articulos']:
            request.session['articulos'][id]['cantidad'] = request.session['articulos'][id]['cantidad'] + 1
        else:
            request.session['articulos'][id] = {'precio':float(articulo.precio), 'cantidad': 1}

    print(request.session['articulos'])
    return redirect('articulos:lista')


class ReporteArticulosPDF(View) : 
    def encabezado(self,pdf):
        pdf.setFont("Helvetica", 16)
        pdf.drawString(230, 790, u"HOSPITAL MATASANOS")
        pdf.setFont("Helvetica", 14)
        pdf.drawString(200, 770, u"LISTA DE PRECIO DE ARTICULOS")  

    def tabla(self, pdf,y):
        encabezados = ('Clave', 'Nombre', 'Precio', 'Stock')
        misArticulos= Articulo.objects.all()
        detalles = []
        for articulo in misArticulos:
            detalles.append((articulo.clave,articulo.nombre,articulo.precio,articulo.stock))
        detalles.reverse()
        detalle_orden = Table([encabezados] + detalles, colWidths=[3 * cm, 6 * cm, 4 * cm, 4 * cm])

        detalle_orden.setStyle(TableStyle(
            [
                ('ALIGN',(0,0),(2,0),'CENTER'),
                ('GRID', (0, 0), (-1, -1),2 , colors.black), 
                ('FONTSIZE', (0, 0), (-1, -1), 10),
            ]
        ))
        detalle_orden.wrapOn(pdf, 800, 600)
        detalle_orden.drawOn(pdf, 60,y)

    def get(self, request, *args, **kwargs):
        response = HttpResponse(content_type='application/pdf')
        buffer = BytesIO()
        pdf = canvas.Canvas(buffer)
        self.encabezado(pdf)
        y = 600
        self.tabla(pdf, y)
        pdf.showPage()
        pdf.save()
        pdf = buffer.getvalue()
        buffer.close()
        response.write(pdf)
        return response    

        